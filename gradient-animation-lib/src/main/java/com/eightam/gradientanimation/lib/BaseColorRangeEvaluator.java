package com.eightam.gradientanimation.lib;

import android.graphics.drawable.GradientDrawable;
import android.view.View;

public abstract class BaseColorRangeEvaluator<T extends ColorRange>
        implements ColorRangeEvaluator<T> {

    private GradientDrawable drawable;
    private View view;

    public BaseColorRangeEvaluator(ColorRange startValue, View view) {
        this.drawable = new GradientDrawable(
                GradientDrawable.Orientation.TOP_BOTTOM,
                startValue.toArray());

        this.drawable.setDither(false);
        this.drawable.setShape(GradientDrawable.RECTANGLE);
        this.drawable.setGradientType(GradientDrawable.LINEAR_GRADIENT);

        this.view = view;
    }

    @Override
    public T evaluate(float fraction, T startValue, T endValue) {
        T colorRange = evaluateInternal(fraction, startValue, endValue);

        drawable.setColors(colorRange.toArray());
        view.setBackground(drawable);

        return colorRange;
    }

    protected abstract T evaluateInternal(float fraction, T startValue, T endValue);

}
