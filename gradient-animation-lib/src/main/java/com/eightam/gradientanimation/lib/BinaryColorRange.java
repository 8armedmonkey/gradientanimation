package com.eightam.gradientanimation.lib;

public class BinaryColorRange implements ColorRange {

    private int startColor;
    private int endColor;

    public BinaryColorRange(int startColor, int endColor) {
        this.startColor = startColor;
        this.endColor = endColor;
    }

    public BinaryColorRange(BinaryColorRange colorRange) {
        this.startColor = colorRange.startColor;
        this.endColor = colorRange.endColor;
    }

    @Override
    public int getStartColor() {
        return startColor;
    }

    @Override
    public void setStartColor(int startColor) {
        this.startColor = startColor;
    }

    @Override
    public int getEndColor() {
        return endColor;
    }

    @Override
    public void setEndColor(int endColor) {
        this.endColor = endColor;
    }

    @Override
    public int[] toArray() {
        return new int[]{startColor, endColor};
    }

}
