package com.eightam.gradientanimation.lib;

import android.animation.TypeEvaluator;

public interface ColorRangeEvaluator<T extends ColorRange>
        extends TypeEvaluator<T> {
}
