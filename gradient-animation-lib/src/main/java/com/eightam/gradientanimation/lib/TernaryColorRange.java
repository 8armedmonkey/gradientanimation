package com.eightam.gradientanimation.lib;

public class TernaryColorRange implements ColorRange {

    private int startColor;
    private int middleColor;
    private int endColor;

    public TernaryColorRange(int startColor, int middleColor, int endColor) {
        this.startColor = startColor;
        this.middleColor = middleColor;
        this.endColor = endColor;
    }

    public TernaryColorRange(TernaryColorRange colorRange) {
        this.startColor = colorRange.startColor;
        this.middleColor = colorRange.middleColor;
        this.endColor = colorRange.endColor;
    }

    @Override
    public int getStartColor() {
        return startColor;
    }

    @Override
    public void setStartColor(int startColor) {
        this.startColor = startColor;
    }

    @Override
    public int getEndColor() {
        return endColor;
    }

    @Override
    public void setEndColor(int endColor) {
        this.endColor = endColor;
    }

    @Override
    public int[] toArray() {
        return new int[]{startColor, middleColor, endColor};
    }

    public int getMiddleColor() {
        return middleColor;
    }

    public void setMiddleColor(int middleColor) {
        this.middleColor = middleColor;
    }

}
