package com.eightam.gradientanimation.lib;

import android.animation.ArgbEvaluator;
import android.view.View;

public class TernaryColorRangeEvaluator extends BaseColorRangeEvaluator<TernaryColorRange> {

    private ArgbEvaluator startColorEvaluator;
    private ArgbEvaluator middleColorEvaluator;
    private ArgbEvaluator endColorEvaluator;
    private TernaryColorRange range;

    public TernaryColorRangeEvaluator(TernaryColorRange startValue, View view) {
        super(startValue, view);
        startColorEvaluator = new ArgbEvaluator();
        middleColorEvaluator = new ArgbEvaluator();
        endColorEvaluator = new ArgbEvaluator();
        range = new TernaryColorRange(startValue);
    }

    @Override
    protected TernaryColorRange evaluateInternal(
            float fraction,
            TernaryColorRange startValue,
            TernaryColorRange endValue) {

        int startColor = (int) (startColorEvaluator.evaluate(
                fraction, startValue.getStartColor(), endValue.getStartColor()));

        int middleColor = (int) (middleColorEvaluator.evaluate(
                fraction, startValue.getMiddleColor(), endValue.getMiddleColor()));

        int endColor = (int) (endColorEvaluator.evaluate(
                fraction, startValue.getEndColor(), endValue.getEndColor()));

        range.setStartColor(startColor);
        range.setMiddleColor(middleColor);
        range.setEndColor(endColor);

        return range;
    }
}
