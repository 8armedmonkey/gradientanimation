package com.eightam.gradientanimation.lib;

public interface ColorRange {

    int getStartColor();

    void setStartColor(int startColor);

    int getEndColor();

    void setEndColor(int endColor);

    int[] toArray();

}
