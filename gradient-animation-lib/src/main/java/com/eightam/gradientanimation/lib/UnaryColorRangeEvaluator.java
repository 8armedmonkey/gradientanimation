package com.eightam.gradientanimation.lib;

import android.animation.ArgbEvaluator;
import android.view.View;

public class UnaryColorRangeEvaluator extends BaseColorRangeEvaluator<UnaryColorRange> {

    private ArgbEvaluator argbEvaluator;
    private UnaryColorRange range;

    public UnaryColorRangeEvaluator(UnaryColorRange startValue, View view) {
        super(startValue, view);
        argbEvaluator = new ArgbEvaluator();
        range = new UnaryColorRange(startValue);
    }

    @Override
    protected UnaryColorRange evaluateInternal(
            float fraction,
            UnaryColorRange startValue,
            UnaryColorRange endValue) {

        int color = (int) argbEvaluator.evaluate(
                fraction, startValue.getStartColor(), endValue.getStartColor());

        range.setStartColor(color);

        return range;
    }

}
