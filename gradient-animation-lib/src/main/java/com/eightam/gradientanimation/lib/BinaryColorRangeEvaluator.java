package com.eightam.gradientanimation.lib;

import android.animation.ArgbEvaluator;
import android.view.View;

public class BinaryColorRangeEvaluator extends BaseColorRangeEvaluator<BinaryColorRange> {

    private ArgbEvaluator startColorEvaluator;
    private ArgbEvaluator endColorEvaluator;
    private BinaryColorRange range;

    public BinaryColorRangeEvaluator(BinaryColorRange startValue, View view) {
        super(startValue, view);
        startColorEvaluator = new ArgbEvaluator();
        endColorEvaluator = new ArgbEvaluator();
        range = new BinaryColorRange(startValue);
    }

    @Override
    protected BinaryColorRange evaluateInternal(
            float fraction,
            BinaryColorRange startValue,
            BinaryColorRange endValue) {

        int startColor = (int) (startColorEvaluator.evaluate(
                fraction, startValue.getStartColor(), endValue.getStartColor()));

        int endColor = (int) (endColorEvaluator.evaluate(
                fraction, startValue.getEndColor(), endValue.getEndColor()));

        range.setStartColor(startColor);
        range.setEndColor(endColor);

        return range;
    }

}
