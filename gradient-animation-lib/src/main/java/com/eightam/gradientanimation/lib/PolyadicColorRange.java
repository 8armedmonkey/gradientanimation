package com.eightam.gradientanimation.lib;

public class PolyadicColorRange implements ColorRange {

    private int[] colors;

    public PolyadicColorRange(int... colors) {
        if (colors == null || colors.length == 0) {
            throw new IllegalArgumentException();
        }

        this.colors = new int[colors.length];
        System.arraycopy(colors, 0, this.colors, 0, colors.length);
    }

    public PolyadicColorRange(PolyadicColorRange range) {
        this(range.colors);
    }

    @Override
    public int getStartColor() {
        return colors[0];
    }

    @Override
    public void setStartColor(int startColor) {
        this.colors[0] = startColor;
    }

    @Override
    public int getEndColor() {
        return colors[colors.length - 1];
    }

    @Override
    public void setEndColor(int endColor) {
        this.colors[colors.length - 1] = endColor;
    }

    @Override
    public int[] toArray() {
        return colors;
    }

}
