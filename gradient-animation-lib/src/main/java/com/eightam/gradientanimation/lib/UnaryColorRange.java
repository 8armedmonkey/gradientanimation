package com.eightam.gradientanimation.lib;

public class UnaryColorRange implements ColorRange {

    private int color;

    public UnaryColorRange(int color) {
        this.color = color;
    }

    public UnaryColorRange(UnaryColorRange range) {
        this.color = range.color;
    }

    @Override
    public int getStartColor() {
        return color;
    }

    @Override
    public void setStartColor(int startColor) {
        this.color = startColor;
    }

    @Override
    public int getEndColor() {
        return color;
    }

    @Override
    public void setEndColor(int endColor) {
        this.color = endColor;
    }

    @Override
    public int[] toArray() {
        return new int[]{color};
    }

}
