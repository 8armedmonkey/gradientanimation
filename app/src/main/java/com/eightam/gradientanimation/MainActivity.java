package com.eightam.gradientanimation;

import android.animation.ValueAnimator;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.eightam.gradientanimation.lib.TernaryColorRange;
import com.eightam.gradientanimation.lib.TernaryColorRangeEvaluator;

public class MainActivity extends AppCompatActivity {

    private static final int MAX_SAMPLE_END_VALUE_INDEX = 1;

    private int sampleEndValueIndex;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initializeViews();
    }

    private void initializeViews() {
        View content = findViewById(R.id.content);

        if (content != null) {
            content.setOnClickListener((view) -> {

                updateSampleEndValueIndex();
                startAnimation(view);

            });
        }
    }

    private void updateSampleEndValueIndex() {
        sampleEndValueIndex = sampleEndValueIndex + 1;

        if (sampleEndValueIndex > MAX_SAMPLE_END_VALUE_INDEX) {
            sampleEndValueIndex = 0;
        }
    }

    private void startAnimation(View view) {
        TernaryColorRange startValue = new TernaryColorRange(0xfff5f5f5, 0xfff5f5f5, 0xfff5f5f5);
        TernaryColorRange endValue;

        if (sampleEndValueIndex == 0) {
            endValue = new TernaryColorRange(0xfff5f5f5, 0xff70bfb1, 0xff01920e);
        } else {
            endValue = new TernaryColorRange(0xfff5f5f5, 0xfffaae7b, 0xffff6600);
        }

        TernaryColorRangeEvaluator evaluator = new TernaryColorRangeEvaluator(startValue, view);

        ValueAnimator.ofObject(evaluator, startValue, endValue)
                .setDuration(800)
                .start();
    }

}
